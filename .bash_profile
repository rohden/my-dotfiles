#
# ~/.bash_profile
#

#[[ -f ~/.bashrc ]] && . ~/.bashrc

# load aliases for the profile
source ${HOME}/.aliasrc

# update the path to include /opt
export PATH="${PATH}:/opt:${HOME}/.local/bin"

export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:/usr/local/lib/pkgconfig"

# set the visudo editor to vim
export EDITOR='vim'

# zsh will alias gbs to git bisect, get rid of that
# if we reload the configuration later, this will cause an error we can ignore
unalias gbs 2>/dev/null

gbs() {
	pattern="${1}"
	if [[ -z "${pattern}" ]] ;
	then
		echo "No branch selected!"
	else
		branch_name=$(git branch -l | grep "${pattern}" | awk '{print $1; exit;}')
		git switch ${branch_name}
	fi
}

# rdp to work machine
connect_to_work(){
	xfreerdp \
          /multimon \
	  /u:#TODO  \
	  /p:$(kwallet-query -r #TODO kdewallet) \
          /v:#TODO \
          /client-hostname:#TODO \
	  /bpp:8 \ 
	  /compression \
	  -themes \
	  -wallpaper \
	  /audio-mode:1 \
	  /network:modem
}

# remove all duplicate entries from the path
if [ -n "$PATH" ]; then
  old_PATH=$PATH:; PATH=
  while [ -n "$old_PATH" ]; do
    x=${old_PATH%%:*}       # the first remaining entry
    case $PATH: in
      *:"$x":*) ;;          # already there
      *) PATH=$PATH:$x;;    # not there yet
    esac
    old_PATH=${old_PATH#*:}
  done
  PATH=${PATH#:}
  unset old_PATH x
fi

